﻿var Logic = function(Helper){
var Model = require('./Model');
var Logic = this;

//Движение пули
this.MoveBulet = function(TimeComplite, CoordGobjects, GameController){
	TimeComplite = TimeComplite / 100 || 1;
	var newX = this.X() + this.dx * TimeComplite;
	var newY = this.Y() + this.dy * TimeComplite;
	var newLocation = Helper.LocationQuard2(newX, newY, this.img.size[0], this.img.size[1]);
	var EquallyLoctionQuard = Helper.EquallyArrays(this.coordQuard, newLocation); //равенство текущих и новых координат (bool)
	var distanceGoal = Helper.Distance(newX, newY, this.X(), this.Y()); //дистанция до цели

	//Трассировка
	this.ids_Contact[this.id] = true;
	var ids_Complete = Helper.Clone(this.ids_Contact);
	var ObjectsContact = []; //Объекты в квадратах, через которые пролетает пуля
	var Coords = []; //Проверенные квадраты

	do
	{
		//Проверка на столкновение с объектами в квадратах
		for(var q in this.coordQuard) //все текущие квадраты
			if(Coords.indexOf(this.coordQuard[q]) == -1) //такой квадрат ещё не проверялся
			{
				for(var i in CoordGobjects[this.coordQuard[q]])
				{
					if(CoordGobjects[this.coordQuard[q]][i].typeClass != "Bulet" && !ids_Complete[CoordGobjects[this.coordQuard[q]][i].id] && CoordGobjects[this.coordQuard[q]][i].physics == true)
					{
						CoordsContact = Logic.CoordContactTrajectoryWithObject(this, CoordGobjects[this.coordQuard[q]][i], newX, newY);
						if(CoordsContact.length != 0)
							ObjectsContact.push([CoordGobjects[this.coordQuard[q]][i], CoordsContact[Math.floor(CoordsContact.length / 2)]]); //добавляем в список контактов с координатами столкновения
					}
					ids_Complete[CoordGobjects[this.coordQuard[q]][i].id] = true; //объект обработан
				}
				Coords.push(this.coordQuard[q]); //в список проверенных квадратов
			}

		if(!EquallyLoctionQuard) //изменения в новых координатах
		{
			var cNet = Logic.CoordContactTrajectoryWithNet(this, newX, newY); //переместить объект на место пересечения с осями

			//новое местоположение
			this.coordQuard = Helper.LocationQuard2(cNet[0], cNet[1], this.img.size[0], this.img.size[1]);
			EquallyLoctionQuard = Helper.EquallyArrays(this.coordQuard, newLocation); //равенство текущих и новых координат (bool)
			this.X(cNet[0]);
			this.Y(cNet[1]);
		}
		else
		{
			this.X(newX);
			this.Y(newY);
		}
	}while((this.X() != newX || this.Y() != newY) && !(this.X() < 0 || this.Y() < 0 || this.X() > Helper.SizeMap[0] || this.Y() > Helper.SizeMap[1] || Helper.Distance(this.X(), this.Y(), this.startX, this.startY) > this.range)); //До тех пор, пока не дошли до квадрата назначения

	//проход по всем найденным объектам столкновения
	var objectContact = false;
	if(ObjectsContact.length != 0)
		objectContact = ObjectsContact[0];

	if(ObjectsContact.length > 1) //больше 1 столкновения
	{
		min = ObjectsContact[0]; //минимально близкий объект
		for(var o in ObjectsContact)
		{
			ObjectsContact[o][2] = Helper.Distance(this.X(), this.Y(), ObjectsContact[o][1][0], ObjectsContact[o][1][1]);
			if(ObjectsContact[o][2] < min[2])
				min = ObjectsContact[o];
		}

		objectContact = min;
	}

	if(objectContact)
	{
		objectContact[0].AddEffect("Damage", {damage: this.damage, damageArmor: this.damageArmor, idCreator: this.idCreator});

		this.damageArmor -= objectContact[0].Armor();
		if(this.damageArmor < 0)
			this.damageArmor = 0;
		this.damage -= Math.sqrt(objectContact[0].img.size[0] * objectContact[0].img.size[1]) * (objectContact[0].Armor() + 1) / (this.damageArmor + 1);

		this.ids_Contact[objectContact[0].id] = true; //есть попадание

		if(this.imgsContact[objectContact[0].typeStructure]) //анимация контакта с данным типом, если есть
		{
			var AnimateContact = new Model.Animate(this.imgsContact[objectContact[0].typeStructure], objectContact[1][0], objectContact[1][1], this.RotateAngle() - (90 + Helper.getRandom(-3, 3)), 1, 100, true);
			AnimateContact.Move = Logic.MoveAnimated; //анимация имеет время жизни
			GameController.AddGObject(AnimateContact); //Добавление анимации контакта
		}

		if(this.damage <= 0)
		{
			this.Delet(true); //удаляем пулю
			return;
		}
	}

	if(this.X() < 0 || this.Y() < 0 || this.X() > Helper.SizeMap[0] || this.Y() > Helper.SizeMap[1] || Helper.Distance(this.X(), this.Y(), this.startX, this.startY) > this.range)
		this.Delet(true);
}

//Возвращает место контакта траетории объекта a с объектом b
this.CoordContactTrajectoryWithObject = function(a, b, aEndX, aEndY){ //aEndX, aEndY - конечная точка траектории
	var coords = [];

	for(var i = 0; i < a.PointRay().length; i++)
	{
		var cy = -1, cx = -1, x, y;
		if(a.RotateAngle() >= 0 && a.RotateAngle() <= 90)
		{
			x = b.X() - b.img.size[0] / 2;
			cy = a.Fy(x, i); //y
			if(cy - a.img.size[1] / 2 > aEndY || cy < a.Y() - a.img.size[1] / 2) //за пределами точки назначения или сзади объекта
				cy = -1;

			y = b.Y() - b.img.size[1] / 2;
			cx = a.Fx(y, i); //x
			if(cx - a.img.size[0] / 2 > aEndX || cx < a.X() - a.img.size[0] / 2) //за пределами точки назначения или сзади объекта
				cx = -1;
		}
		else if(a.RotateAngle() > 90 && a.RotateAngle() <= 180)
		{
			x = b.X() + b.img.size[0] / 2;
			cy = a.Fy(x, i); //y
			if(cy - a.img.size[1] / 2 > aEndY || cy < a.Y() - a.img.size[1] / 2) //за пределами точки назначения
				cy = -1;

			y = b.Y() - b.img.size[1] / 2;
			cx = a.Fx(y, i); //x
			if(cx + a.img.size[0] / 2 < aEndX || cx > a.X() + a.img.size[0] / 2) //за пределами точки назначения
				cx = -1;
		}
		else if(a.RotateAngle() > 180 && a.RotateAngle() <= 270)
		{
			x = b.X() + b.img.size[0] / 2;
			cy = a.Fy(x, i); //y
			if(cy + a.img.size[1] / 2 < aEndY || cy > a.Y() + a.img.size[1] / 2) //за пределами точки назначения
				cy = -1;

			y = b.Y() + b.img.size[1] / 2;
			cx = a.Fx(y, i); //x
			if(cx + a.img.size[0] / 2 < aEndX || cx > a.X() + a.img.size[0] / 2) //за пределами точки назначения
				cx = -1;
		}
		else
		{
			x = b.X() - b.img.size[0] / 2;
			cy = a.Fy(x, i); //y
			if(cy + a.img.size[1] / 2 < aEndY || cy > a.Y() + a.img.size[1] / 2) //за пределами точки назначения
				cy = -1;

			y = b.Y() + b.img.size[1] / 2;
			cx = a.Fx(y, i); //x
			if(cx - a.img.size[0] / 2 > aEndX || cx < a.X() - a.img.size[0] / 2) //за пределами точки назначения
				cx = -1;
		}

		if(cy != -1 && cy < b.Y() + b.img.size[1] / 2 && cy > b.Y() - b.img.size[1] / 2) //в пределах объекта b
			coords.push([x, cy]);
		else if(cx != -1 && cx < b.X() + b.img.size[0] / 2 && cx > b.X() - b.img.size[0] / 2) //в пределах объекта b
			coords.push([cx, y]);
	}
	return coords;
}

//Возвращает место контакта траектории объекта a с сеткой
this.CoordContactTrajectoryWithNet = function(a, aEndX, aEndY){ //aEndX, aEndY - конечная точка траектории
	var cy = -1, cx = -1, middleRay = Math.floor(a.PointRay().length / 2), x, y;
	if(a.RotateAngle() >= 0 && a.RotateAngle() <= 90)
	{
		x = a.X() + (Helper.sizeQuard - a.X() % Helper.sizeQuard);
		cy = a.Fy(x, middleRay); //y

		y = a.Y() + (Helper.sizeQuard - a.Y() % Helper.sizeQuard);
		cx = a.Fx(y, middleRay); //x

		if(cy >= aEndY && cx >= aEndX) //за пределами точки назначения
			return [aEndX, aEndY];
		if(Helper.Distance(a.X(), a.Y(), x, cy) < Helper.Distance(a.X(), a.Y(), cx, y)) //самая близкая точка - искомая
			return [x, cy];
		return [cx, y];
	}
	else if(a.RotateAngle() > 90 && a.RotateAngle() <= 180)
	{
		x = a.X() - a.X() % Helper.sizeQuard;
		cy = a.Fy(x, middleRay); //y

		y = a.Y() + (Helper.sizeQuard - a.Y() % Helper.sizeQuard);
		cx = a.Fx(y, middleRay); //x

		if(cy >= aEndY && cx <= aEndX) //за пределами точки назначения
			return [aEndX, aEndY];
		if(Helper.Distance(a.X(), a.Y(), x, cy) < Helper.Distance(a.X(), a.Y(), cx, y))
			return [x - 0.01, a.Fy(x - 0.01, middleRay)];
		return [a.Fx(y + 0.01, middleRay), y + 0.01];
	}
	else if(a.RotateAngle() > 180 && a.RotateAngle() <= 270)
	{
		x = a.X() - a.X() % Helper.sizeQuard;
		cy = a.Fy(x, middleRay); //y

		y = a.Y() - a.Y() % Helper.sizeQuard;
		cx = a.Fx(y, middleRay); //x

		if(cy < aEndY && cx < aEndX) //за пределами точки назначения
			return [aEndX, aEndY];
		if(Helper.Distance(a.X(), a.Y(), x, cy) < Helper.Distance(a.X(), a.Y(), cx, y))
			return [x - 0.01, a.Fy(x - 0.01, middleRay)];
		return [a.Fx(y - 0.01, middleRay), y - 0.01];
	}
	else
	{
		x = a.X() + (Helper.sizeQuard - a.X() % Helper.sizeQuard);
		cy = a.Fy(x, middleRay); //y

		y = a.Y() - a.Y() % Helper.sizeQuard;
		cx = a.Fx(y, middleRay); //x

		if(cy < aEndY && cx > aEndX) //за пределами точки назначения
			return [aEndX, aEndY];
		if(Helper.Distance(a.X(), a.Y(), x, cy) < Helper.Distance(a.X(), a.Y(), cx, y))
			return [x + 0.01, a.Fy(x + 0.01, middleRay)];
		return [a.Fx(y - 0.01, middleRay), y - 0.01];
	}
}


//Жизнь анимации
this.MoveAnimated = function(TimeComplite){
	if(this.view[0] == "a" && this.residueTimeLife != this.frimesLife * this.img.timeChangeFrame +this. timeLife + 10)
		this.view = Helper.generateView(this);

	this.residueTimeLife -= TimeComplite;

	if(this.residueTimeLife <= 0) //время жизни кончилось
		this.Delet(true); //удаляем объект

	if(this.residueTimeLife <= this.timeLife + 10) //время гибели
	{
		this.changeState = true; //постоянная отправка клиенту на прорисовку затухания
		if(this.view[0] == "i")
			this.view = Helper.generateView(this);
	}
};

/* Разбросы  */
//Нарастающий
this.ScatterIncreasing = function(rotateAngle, max, timeLast, forceGamer, maxGet){ //timeLast - время последнего вызова
	var TimeRecovery = 1000 / (forceGamer / 2); //Полное время восстановления
	var ForceTime = TimeRecovery - timeLast; //сила времени
	if(ForceTime < 0)
		ForceTime = 0;

	max = max / forceGamer * (ForceTime / TimeRecovery);

	if(max == 0)
		return rotateAngle;

	if(maxGet)
		return rotateAngle + max;

	return Helper.getRandom(rotateAngle - max, rotateAngle + max);
}

//Равномерный
this.ScatterUniform = function(rotateAngle, max){
	return Helper.getRandom(rotateAngle - max, rotateAngle + max);
}

//Центрированный
this.ScatterCentric = function(rotateAngle, max, forceCenter){

}

this.BottomGamerRotate = function(el, args){
	if(args.keys.indexOf('38') != -1 || args.keys.indexOf('87') != -1) //w
	{
		if(args.keys.indexOf('39') != -1 || args.keys.indexOf('68') != -1) //d
		{
			if(el.RotateAngle() > 45 && el.RotateAngle() < 225)
				el.bottom.RotateAngle(135);
			else
				el.bottom.RotateAngle(-45);
		}
		else if(args.keys.indexOf('37') != -1 || args.keys.indexOf('65') != -1) //a
		{
			if(el.RotateAngle() > 315 && el.RotateAngle() < 135)
				el.bottom.RotateAngle(45);
			else
				el.bottom.RotateAngle(-135);
		}
		else
		if(el.RotateAngle() > 0 && el.RotateAngle() < 180)
			el.bottom.RotateAngle(90);
		else
			el.bottom.RotateAngle(-90);
	}
	else if(args.keys.indexOf('40') != -1 || args.keys.indexOf('83') != -1) //s
	{
		if(args.keys.indexOf('39') != -1 || args.keys.indexOf('68') != -1) //d
		{
			if(el.RotateAngle() < 45 || el.RotateAngle() > 225)
				el.bottom.RotateAngle(45);
			else
				el.bottom.RotateAngle(225);
		}
		else if(args.keys.indexOf('37') != -1 || args.keys.indexOf('65') != -1) //a
		{
			if(el.RotateAngle() < 315 || el.RotateAngle() > 135)
				el.bottom.RotateAngle(-45);
			else
				el.bottom.RotateAngle(135);
		}
		else
		if(el.RotateAngle() > 0 && el.RotateAngle() < 180)
			el.bottom.RotateAngle(90);
		else
			el.bottom.RotateAngle(-90);
	}
	else if(args.keys.indexOf('39') != -1 || args.keys.indexOf('68') != -1) //d
	{
		if(el.RotateAngle() > 90 && el.RotateAngle() < 270)
			el.bottom.RotateAngle(180);
		else
			el.bottom.RotateAngle(0.00000001);
	}
	else if(args.keys.indexOf('37') != -1 || args.keys.indexOf('65') != -1) //a
	{
		if(el.RotateAngle() < 90 || el.RotateAngle() > 270)
			el.bottom.RotateAngle(0.000000001);
		else
			el.bottom.RotateAngle(180);
	}
}

this.Abilitys = []; //Способности
this.Abilitys["Strike"] = function(weapon){ //Стрельба
	this.Click = function(GameController, Omain){ //Omain та сущность что владеет этой способностью
		if(new Date().getTime() - this.weapon.TimeRateOf >= this.weapon.rateOf) //можно атаковать
		{
			if(this.weapon.Bulet) //если есть боеприпас
			{
				//настройка пули
				var bulet = Helper.CloneClass(this.weapon.Bulet);
				bulet.idCreator = Omain.id; //создатель пули
				bulet.X(Omain.X() + Omain.pointCreateWeaponXY[0] + this.weapon.pointCreateBuletXY[0] + this.weapon.img.size[0] / 2);
				bulet.Y(Omain.Y() + Omain.pointCreateWeaponXY[1] + this.weapon.pointCreateBuletXY[1] + this.weapon.img.size[1] / 2);
				if(Omain.scatter)
					bulet.X(bulet.X() - Omain.scatter.CurrentFrame() * 2 + 2);

				var XYCoord = Helper.Coord(Omain.X(), Omain.Y(), bulet.X(), bulet.Y(), Omain.RotateAngle()); //расчёт где пуля должна появиться вокруг игрока
				bulet.X(XYCoord[0]);
				bulet.Y(XYCoord[1]);
				bulet.startX = XYCoord[0];
				bulet.startY = XYCoord[1];
				bulet.RotateAngle(Helper.RotateAngle(Omain.X(), Omain.Y(), Omain.AimXY())); //+ 90 если патрон вертикально стоит
				bulet.speed = this.weapon.speedShot;
				bulet.Move = Logic.MoveBulet;
				bulet.effects = []; //Дабы ссылку заменить от наследования
				//Разброс
				var scatter = Logic.ScatterIncreasing(bulet.RotateAngle(), this.weapon.scatterMax, new Date().getTime() - this.weapon.TimeRateOf, Omain.force); //отклонение для данной пули

				if(scatter != bulet.RotateAngle()) //если есть разброс
				{
					if(Omain.imgScatter && !Omain.scatter) //есть картинка отдачи но нет объекта
					{
						var ScatterAnimate = new Model.Animate(Omain.imgScatter, Omain.X(), Omain.Y(), Omain.RotateAngle());
						ScatterAnimate.animate = false;

						Omain.scatter = ScatterAnimate;
						Omain.AddLinkObject(ScatterAnimate, ["MovingKeys", "Rotate"]);
						GameController.AddGObject(ScatterAnimate, Omain.id + "_scatter");
						Omain.Draw(false); //прорисовка главной картинки отключена

						for(var l in Omain.linkObjects) //убираем у оружия из зависимости все эффекты(оно теперь будет контролироваться эффектом Scatter)
							if(Omain.linkObjects[l].el.id == this.weapon.id)
							{
								delete Omain.linkObjects[l].args.dx;
								delete Omain.linkObjects[l].args.dy;
								break;
							}
					}
					if(!Omain.effects["Scatter"] || Omain.effects["Scatter"].scatter < 5)
						Omain.AddEffect("Scatter", {scatter: this.weapon.scatterMax * 0.3}) //добавляем эффект разброса пуль

					bulet.RotateAngle(scatter); //меняем поворот пули от полученного отклонения
				}
				var aimXYScatter = Helper.CoordRotate(Omain.X(), Omain.Y(), Omain.AimXY()[0], Omain.AimXY()[1], scatter);

				//Место появления
				var distance = Helper.Distance(aimXYScatter[0], aimXYScatter[1], bulet.X(), bulet.Y());
				if(distance <= 100) //если малое расстояние, то стреляем прямо, что бы не искажать траекторию пуль
				{
					//коэффициент подобия треугольников на основе радиусов, а не гипотенузы
					var k = Helper.Distance(aimXYScatter[0], aimXYScatter[1], Omain.X(), Omain.Y()) / bulet.speed;
					bulet.dx = (aimXYScatter[0] - Omain.X()) / k; //приращение за 1 ход
					bulet.dy = (aimXYScatter[1] - Omain.Y()) / k;
				}
				else
				{
					//коэффициент подобия треугольников на основе радиусов, а не гипотенузы
					var k = distance / bulet.speed;
					bulet.dx = (aimXYScatter[0] - bulet.X()) / k; //приращение за 1 ход
					bulet.dy = (aimXYScatter[1] - bulet.Y()) / k;
				}

				//траектория пули
				bulet.pr = [];
				if(bulet.width > 5)
				{
					//крайние точки для создания крайних лучей
					bulet.PointRay(Helper.CoordInRightTriangle(bulet.X(), bulet.Y(), bulet.RotateAngle() - 90, bulet.img.size[0] / 2));
					bulet.PointRay(Helper.CoordInRightTriangle(bulet.X(), bulet.Y(), bulet.RotateAngle() + 90, bulet.img.size[0] / 2));
					if(bulet.width > 15)
						bulet.PointRay([bulet.X(), bulet.Y()]); //центральная линия
				}
				else
					bulet.PointRay([bulet.X(), bulet.Y()]); //центральная линия

				bulet.delet = false;
				//if(bulet.dx != int) //TODO: реализоват исключение
				//bulet.delet = true;
				bulet.damage += this.weapon.damage;
				bulet.ids_Contact = {};
				bulet.ids_Contact[bulet.idCreator] = true;
				GameController.AddGObject(bulet); //добавляем пулю

				var AnimateShot = new Model.Animate(this.weapon.imgShot, bulet.X(), bulet.Y(), bulet.RotateAngle() - 90, 1, 100, true);
				AnimateShot.Move = Logic.MoveAnimated; //анимация имеет время жизни
				this.weapon.AddLinkObject(AnimateShot, ["Rotate", "MovingKeys"], {dx: this.weapon.pointCreateBuletXY[0], dy: this.weapon.pointCreateBuletXY[1], angle: 90}); //связь
				GameController.AddGObject(AnimateShot); //Добавление анимации выстрела
				GameController.AddSound(bulet.X(), bulet.Y(), this.weapon.soundShot); //добавляем звук выстрела
			}
			else //ближняя атака
			{

			}
			this.weapon.TimeRateOf = new Date().getTime(); //время атаки
		}
	}
}

this.Effects = []; //Накладвываемые эффекты:    название => функция(элемент, аргументы функции, CoordGobjects, TimeComplite)
this.Effects["MovingJoinObject"] = function(elParent, elChildren, args){ //Перемещение прикреплённого объекта
	args = args ? args : {dx: 0, dy: 0};

	elChildren.X(elParent.X() + args.dx);
	elChildren.Y(elParent.Y() + args.dy);

	//все зависимые объекты
	for(var i in elChildren.linkObjects)
		if(elChildren.linkObjects[i].arrayLinkEffects.indexOf("MovingKeys") != -1) //связаны этим свойством?
			Logic.Effects["MovingJoinObject"](elChildren, elChildren.linkObjects[i].el, elChildren.linkObjects[i].args);
}
this.Effects["MovingKeys"] = function(el, args, CoordGobjects, TimeComplite){ //Движение объекта
	TimeComplite = TimeComplite / 100 || 1;

	for(var i in args.keys)
		switch(args.keys[i])
		{
			case "38": //w
			case "87":
				var array = Helper.Clone(CoordGobjects[el.coordQuard[0]]); //верхний левый угол объекта
				if(el.coordQuard.length > 1 && el.coordQuard[1] < el.coordQuard[0] + Helper.SizeMapQuards[0])
					for(var i in CoordGobjects[el.coordQuard[1]]) //все верхние квадраты расположения
						array[i] = CoordGobjects[el.coordQuard[1]][i];

				if(el.Y() > 0 && (!array || Object.keys(array).length == 0 || !Helper.ContainsArray([el.X() - el.img.size[0] / 2.5, el.Y() - el.img.size[1] / 2.5 - el.speed * TimeComplite, el.img.size[0] / 1.5, el.speed * TimeComplite, el.id], array, true)))
					el.Y(el.Y() - el.speed * TimeComplite);
				break; //W

			case "39": //d
			case "68":
				var array = Helper.Clone(CoordGobjects[el.coordQuard[0]]);
				if(el.coordQuard.length > 1)
				{
					if(el.coordQuard[1] % el.coordQuard[0] == 0)
						array = Helper.Clone(CoordGobjects[el.coordQuard[1]]); //верхний правый угол объекта

					if(el.coordQuard.length - 1 > el.coordQuard[0] + Helper.SizeMapQuards[0])
						for(var i in CoordGobjects[el.coordQuard[el.coordQuard.length - 1]]) //все верхние квадраты расположения
							array[i] = CoordGobjects[el.coordQuard[el.coordQuard.length - 1]][i];
				}

				if(el.X() < Helper.SizeMap[0] && (Object.keys(array).length == 0 || !Helper.ContainsArray([el.X() + el.img.size[0] / 2.5, el.Y() - el.img.size[1] / 2.5, el.speed * TimeComplite, el.img.size[1] / 1.5, el.id], array, true)))
					el.X(el.X() + el.speed * TimeComplite); break; //D

			case "40": //s
			case "83":
				var array = Helper.Clone(CoordGobjects[el.coordQuard[el.coordQuard.length - 1]]); //нижний правый угол объекта
				if(el.coordQuard.length > 1 && el.coordQuard[el.coordQuard.length - 1] > el.coordQuard[0] + Helper.SizeMapQuards[0])
					for(var i in CoordGobjects[el.coordQuard[el.coordQuard.length - 2]]) //все нижние квадраты расположения
						array[i] = CoordGobjects[el.coordQuard[el.coordQuard.length - 2]][i];

				if(el.Y() < Helper.SizeMap[1] && (Object.keys(array).length == 0 || !Helper.ContainsArray([el.X() - el.img.size[0] / 2.5, el.Y() + el.img.size[1] / 2.5, el.img.size[0] / 1.5, el.speed * TimeComplite, el.id], array, true)))
					el.Y(el.Y() + el.speed * TimeComplite);
				break; //S

			case "37": //a
			case "65":
				var array = Helper.Clone(CoordGobjects[el.coordQuard[0]]); //верхний левый угол объекта
				if(el.coordQuard.length > 1 && el.coordQuard[el.coordQuard.length - 1] > el.coordQuard[0] + Helper.SizeMapQuards[0])
				{
					var LeftArea = Helper.Clone(CoordGobjects[el.coordQuard[1]]);
					if(el.coordQuard[0] % el.coordQuard[1] != 0) //второй эл в одном столбце с первым?
						LeftArea = el.coordQuard[2]; //если нет, то третий элемент в том же столбце

					for(var i in LeftArea) //все левые квадраты расположения
						array[i] = LeftArea[i];
				}

				if(el.X() > 0 && (Object.keys(array).length == 0 || !Helper.ContainsArray([el.X() - el.img.size[0] / 2.5 - el.speed * TimeComplite, el.Y() - el.img.size[1] / 2.5, el.speed * TimeComplite, el.img.size[1] / 1.5, el.id], array, true)))
					el.X(el.X() - el.speed * TimeComplite);
				break; //A
		}

	if(el.bottom) //Поворот нижней части
		Logic.BottomGamerRotate(el, args);

	//все зависимые объекты
	for(var i in el.linkObjects)
		if(el.linkObjects[i].arrayLinkEffects.indexOf("MovingKeys") != -1) //связаны этим свойством?
			Logic.Effects["MovingJoinObject"](el, el.linkObjects[i].el, el.linkObjects[i].args);

	delete el.effects["MovingKeys"];
}
this.Effects["MovingKeysNoPhysics"] = function(el, args, CoordGobjects, TimeComplite){ //Движение объекта без учёта каких либо ограничений
	TimeComplite = TimeComplite / 100 || 1;

	for(var i in args.keys)
		switch(args.keys[i])
		{
			case "38": //w
			case "87": el.Y(el.Y() - el.speed * TimeComplite);
				break; //W

			case "39": //d
			case "68": el.X(el.X() + el.speed * TimeComplite); break; //D

			case "40": //s
			case "83": el.Y(el.Y() + el.speed * TimeComplite); break; //S

			case "37": //a
			case "65": el.X(el.X() - el.speed * TimeComplite);
				break; //A
		}

	delete el.effects["MovingKeys"];
}
this.Effects["Scatter"] = function(el, args){ //Отдача от оружия
	if(el.ActiveAbility().weapon && (args.scatter > 0 || args.scatterOld > 0)) //если есть оружие и на нём разброс остался
	{
		args.scatterOld = args.scatter;
		args.scatter -= el.ActiveAbility().weapon.scatterMax / 100; //записываем результат обратно в аргументы

		if(el.imgScatter && el.scatter)
		{
			var weapon = el.ActiveAbility().weapon; //для сокращения записи
			el.scatter.CurrentFrame(parseInt(args.scatter / ((5 + weapon.scatterMax * 0.3) / el.scatter.img.nframes)) + 1);

			//Маним-шаманим, оружие на место ставим
			weapon.X(el.X() + el.pointCreateWeaponXY[0] - el.scatter.CurrentFrame() * 2 + 2 + weapon.img.size[0] / 2);
			weapon.Y(el.Y() + el.pointCreateWeaponXY[1] + weapon.img.size[1] / 2);
			var r = Helper.Distance(weapon.X(), weapon.Y(), el.X(), el.Y());
			var Angle0 = Math.acos((weapon.X() - el.X()) / r) * 180 / Math.PI;
			var XY = Helper.CoordInRightTriangle(el.X(), el.Y(), Angle0 + el.RotateAngle(), r);
			weapon.X(XY[0]);
			weapon.Y(XY[1]);
		}
	}
	else
	{
		if(el.imgScatter && el.scatter)
		{
			for(var l in el.linkObjects) //возвращаем оружию аргумент
				if(el.linkObjects[l].el.id == el.ActiveAbility().weapon.id)
				{
					el.linkObjects[l].args.dx = el.pointCreateWeaponXY[0] + el.ActiveAbility().weapon.img.size[0] / 2;
					el.linkObjects[l].args.dy = el.pointCreateWeaponXY[1] + el.ActiveAbility().weapon.img.size[1] / 2;
					break;
				}
			el.Draw(true); //прорисовка главной картинки
			el.scatter.Delet(true); //удалем анимацию отдачи
			delete el.scatter;
		}
		delete el.effects["Scatter"];
	}
}
this.Effects["RotateJoinObject"] = function(elParent, elChildren, args){ //Вращение прикреплённого объекта
	args = args ? args : {dx: 0, dy: 0};

	elChildren.RotateAngle(elParent.RotateAngle());

	var r = Helper.Distance(args.dx, args.dy, 0, 0);
	var Angle0 = Math.acos(args.dx / r) * 180 / Math.PI;
	var XY = Helper.CoordInRightTriangle(elParent.X(), elParent.Y(), Angle0 + elParent.RotateAngle(), r);
	elChildren.X(XY[0]);
	elChildren.Y(XY[1]);

	//все зависимые объекты
	for(var i in elChildren.linkObjects)
		if(elChildren.linkObjects[i].arrayLinkEffects.indexOf("Rotate") != -1) //связаны этим свойством?
			Logic.Effects["RotateJoinObject"](elChildren, elChildren.linkObjects[i].el, elChildren.linkObjects[i].args);
}
this.Effects["Rotate"] = function(el, args){ //поворот объекта
	if(args.angle && args.angle != el.RotateAngle())
		el.RotateAngle(args.angle);

	//все зависимые объекты
	for(var i in el.linkObjects)
		if(el.linkObjects[i].arrayLinkEffects.indexOf("Rotate") != -1) //связаны этим свойством?
			Logic.Effects["RotateJoinObject"](el, el.linkObjects[i].el, el.linkObjects[i].args);

	delete el.effects["Rotate"];
}
this.Effects["Damage"] = function(el, args, CoordGobjects, TimeComplite, GObjects){ //Нанесение урона
	if(el.Health)
	{
		var residueArmor = el.Armor - args.damageArmor; //остаток брони
		if(residueArmor < 0)
			residueArmor = 0;
		var damage = args.damage - residueArmor;

		if(damage > 0) //если урон остался
		{
			el.Health(el.Health() - damage);

			if(el.rise)  //если это игрок
				if(el.Health() > 0) //выжил
					GObjects[args.idCreator].AddEffect("Score", {score: damage});
				else  //убит
					GObjects[args.idCreator].AddEffect("Score", {score: damage, kill: el.id});
		}
	}
}
this.Effects["Score"] = function(el, args, CoordGobjects, TimeComplite, GObjects){ //счёт игрока
	el.score += args.score;
	if(args.kill)
	{
		if(GObjects[args.kill].rise == el.rise)
		{
			el.score -= 25;
			el.kill -= 1;
		}
		else
		{
			el.score += 25;
			el.kill += 1;
		}
	}
}
}

module.exports = Logic; //делаем файл модулем