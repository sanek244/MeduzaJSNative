﻿//TODO: загрузка начальной картинки/фона

var socket = io.connect('http://localhost:11005'); //соединяемся с сервером
socket.json.emit("hash", getCookie("hashCod")); //отправка на сервер хэш кода из кук

var Storage = {img: [], sound: []}; //Хранилище картинок и звуков
var GObjects = []; //[id object] = {Img} картинки объектов
var view = {}; //визуальная часть [zIndex] = [id objects] = [typeDraw, x, y, rotateAngle]
var TimeDraw = new Date().getTime(); //время прошедшее между прорисовками
var canvas = 0;
var scatterCutrsor = 0;
var XYCursor;

socket.on('hash', function(msg){ //для новых клиентов их хэш-код(id) с сервера(безопасность п 0.1)
	document.cookie = "hashCod=" + msg;
});
		
socket.on('loadCollection', function(msg){ //загрузка всех данных в начале игры
	GObjects = ImagesGet(msg);
});

socket.on('changeCollection', function(msg){ //добавление данных о картинках и музыки объектов
	msg = ImagesGet(msg);
	
	for(var id in msg)
		if(msg[id] == "deleted")
		{
			if(GObjects[id])
				delete GObjects[id];
			DeleteViewObject(id);
			drawAll();
		}
		else
			GObjects[id] = msg[id];
});

socket.on('ping', function(msg){ //пилинг с сервера
	socket.json.emit("ping", true); //ответ серверу
});

	
window.onload = function(){ //страница загружена, можно выполнять js код
	var example = document.getElementById("canvas");
	example.height = window.innerHeight;
	example.width  = window.innerWidth;
	canvas = example.getContext('2d');
	
	socket.on('disconect', function(msg){ //потеря соединения с игроком, msg - его id сокета
		console.log("от нас отсоединился: " + msg);
	});

	socket.on('errors', function(msg){ //ошибки с сервера(безопасность)
		console.log(msg);
		alert(msg);
	});
	
	socket.on('view', function(msg){ //картинка с сервера
		drawAll(msg);
	});
	
	socket.on('sound', function(msg){ //запуск музыки и звуков
		for(var i in msg)
		{
			var audio = new Audio(); // Создаём новый элемент Audio
			audio.src = "sound//" + msg[i].nameSound; // Указываем путь к звуку "клика"
			audio.autoplay = true; // Автоматически запускаем
			audio.volume = msg[i].volume; //громкость
		}
	});
	
	socket.on('scatter', function(msg){ //Разброс
		scatterCutrsor = msg;
		cursorBuild(XYCursor[0], XYCursor[1], scatterCutrsor); //сборка курсора
	});
	
	onkeydown = onkeyup = function(e){ //для одновременных нажатий нескольких клавиш
		e = e || event; 
		socket.json.emit("keyControl", e.type + ":" + e.which); //отправка на сервер тип нажатия и код клавиши  
	}
	
	$(window).mousemove(function(e){ //перемещение мыши
		XYCursor = [e.pageX, e.pageY];
		cursorBuild(e.pageX, e.pageY, scatterCutrsor); //сборка курсора :D
		socket.json.emit("mousemove", e.clientX + ":" + e.clientY);
		clearSelection();
	});
	
	// запрет перемещения картинки курсора 
	$("img").mousedown(function(e){
		if (window.event.stopPropagation) 
			window.event.stopPropagation();
		window.event.cancelBubble = true;
		e.cancelBubble = true;
		e.preventDefault();
	});	
	
	document.onmouseup = document.onmousedown = function(e){ //клик мыши 
		socket.json.emit("click", e.clientX + ":" + e.clientY + ":" + e.type + ":" + e.button); //отправка на сервер координат мыши
	};
	
	//document.oncontextmenu=function(e){return false};
	
	$(window).resize(function(){ //изменение размера окна
		var example = document.getElementById("canvas");
		example.height = window.innerHeight;
		example.width  = window.innerWidth;

		canvas = example.getContext('2d');
		socket.json.emit("resize");
	});
}
function clearSelection() {
    if (window.getSelection) 
		window.getSelection().removeAllRanges();
    else  // старый IE
		document.selection.empty();
}

/*  Основной функционал  */
function drawAll(msg){
	TimeDraw = new Date().getTime() - TimeDraw; //время прошедшее между прорисовками
	canvas.clearRect(0, 0, window.innerWidth, window.innerHeight); 
	drawNet(); //сетка
	
	if(msg)
		for(var i in msg) //принятие новых изменений
		{
			if(!view[GObjects[i].img.zIndex])
				view[GObjects[i].img.zIndex] = {};
			view[GObjects[i].img.zIndex][i] = msg[i];
		}
		
	//прорисовка всего
	for(var z in view)
		for(var i in view[z])
		{
			var data = view[z][i]; //первое - имя функции, после - параметры функции
			
			//Если объект за пределами границ экрана
			if(data[1] < 0 || data[2] < 0 || data[1] > window.innerWidth || data[2] > window.innerHeight)
			{
				DeleteViewObject(i);
				continue;
			}
			
			if(!GObjects[i])
			{
				console.log("нету объекта: " + i + " view: " + data);
				DeleteViewObject(i);
			}
			else
			{
				switch(data[0])
				{
					case "a":  //Рандомный первый кадр
						if(GObjects[i].img.randomStart) 
							GObjects[i].img.currentFrame = getRandomInt(1, GObjects[i].img.nframes);
							
						console.log(i + GObjects[i].img.animate);
						if(GObjects[i].img.animate) //смена типа
							data[0] = "i";
						else
							data[0] = "r";
						
						draw(i, data, TimeDraw); //отрисовка объекта
						break;
					
					case "t": //Затухание объекта
						GObjects[i].img.animate = false; //отключение смены кадра
						
						if(!GObjects[i].timeLife)
							GObjects[i].timeLife = data[6];
						GObjects[i].timeLife -= TimeDraw; //время жизни объекта
						canvas.globalAlpha = GObjects[i].timeLife / data[6];
						draw(i, data, TimeDraw); //отрисовка объекта
						canvas.globalAlpha = 1;
						break;
					
					case "r": //Отрисовка указанного кадра
						GObjects[i].img.currentFrame = data[4]; 
						draw(i, data, TimeDraw); //отрисовка объекта
						break;

					case "i": //Анимированное изображение
						draw(i, data, TimeDraw); //отрисовка объекта
						break;
					
					case "o": //Красняая точка
						canvas.beginPath();
						canvas.fillStyle = "rgba(255,0,0, 0.5)";
						canvas.arc(data[1], data[2], 2, 0, Math.PI*2, true);
						canvas.fill();
						break;
				}
			}
			canvas.rotate(0);
		}
		
	TimeDraw = new Date().getTime();
}

function draw(id, data, TimeDraw){
	var img = GObjects[id].img.images[GObjects[id].img.currentFrame];
	
	if(data[3] != 0)
	{
		canvas.translate(data[1] || 0, data[2] || 0);
		canvas.rotate(data[3] * Math.PI / 180);
	}

	if(GObjects[id].img.animate)
	{
		if(!GObjects[id].img.time) //первый раз
			GObjects[id].img.time = GObjects[id].img.timeChangeFrame;
		
		GObjects[id].img.time -= TimeDraw; //время смены кадра
		if(GObjects[id].img.time < 0)
		{
			GObjects[id].img.currentFrame = GObjects[id].img.currentFrame < GObjects[id].img.nframes ? GObjects[id].img.currentFrame + 1 : 1;
			GObjects[id].img.time = GObjects[id].img.timeChangeFrame;
			if(!GObjects[id].img.lifeObject) //картинка не живущая с физическим объектом
			{
				data[5]--; //количество кадров, которые надо показать
				if(data[5] <= 0)
					data[0] = "t"; //данный объект надо плавно скрыть
			}
		}
	}
	if(data[3] != 0)
	{
		canvas.drawImage(img, -img.width / 2, -img.height / 2);
		canvas.rotate(-data[3] * Math.PI / 180);
		canvas.translate(-data[1] || 0, -data[2] || 0);
	}
	else
		canvas.drawImage(img, data[1] -img.width / 2, data[2]-img.height / 2);
}

/*	Вспомогательные функции */
function DeleteViewObject(id){
	if(GObjects[id])
	{
		delete view[GObjects[id].img.zIndex][id];
		if(Object.keys(view[GObjects[id].img.zIndex]).length == 0)
			delete view[GObjects[id].img.zIndex];
	}
	else
		for(var z in view)
			if(view[z][id])
			{
				delete view[z][id];
				if(Object.keys(view[z]).length == 0)
					delete view[z];
				break;
			}
}

//Получает изображения по параметрам
function ImagesGet(GObjects){
	for(var id in GObjects)
		if(GObjects[id] != "deleted") //если объект не удаляется
			GObjects[id].img.images = GetImages(GObjects[id].img); 
	
	return GObjects;
}

//Возвращает набор изображений по параметрам
function GetImages(img){ 
	if(Storage.img[img.nameImg]) //картинка содержится в хранилище
		return Storage.img[img.nameImg];  //берём из хранилища
	else
	{
		Storage.img[img.nameImg] = [];
		for(var i = 1; i <= img.nframes; i++) //Загрузка картинок в хранилище
		{
			Storage.img[img.nameImg][i] = new Image();
			Storage.img[img.nameImg][i].src = "img\\" + img.path + "\\" + img.nameImg + "_" + i + "." + img.type;
			canvas.drawImage(Storage.img[img.nameImg][i], -10000, -10000); //рисуем, что бы браузер загрузил картинку
		}
		
		return Storage.img[img.nameImg]; //берём из хранилища
	}
}

// возвращает cookie с именем name, если есть, если нет, то пустоту
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : "";
}

function getRandomInt(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function cursorBuild(x, y, scatter){
	$("#cursor_center").css({top: y - 3, left: x - 3});
	$("#cursor_part1").css({top: y - 12 - scatter, left: x - 12 - scatter});
	$("#cursor_part2").css({top: y - 12 - scatter, left: x + 1 + scatter});
	$("#cursor_part3").css({top: y + 1 + scatter, left: x + 1 + scatter});
	$("#cursor_part4").css({top: y + 1 + scatter, left: x - 12 - scatter});
}

function drawNet(){
	//canvas.fillStyle = "rgba(255,0,0, 0.5)";
	for(var i = 0; i < 5; i++) //горизонт
	{
		canvas.beginPath();
		canvas.moveTo(0, i * 200);
		canvas.lineTo(1400, i * 200);
		canvas.stroke();
	}
	for(var i = 0; i < 10; i++) //вертикаль
	{
		canvas.beginPath();
		canvas.moveTo(i * 200, 0);
		canvas.lineTo(i * 200, 700);
		canvas.stroke();
	}
	
}