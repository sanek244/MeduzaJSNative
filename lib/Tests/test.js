var Helper = require("../Helper.js")
     describe('Helper', function(){
        it('should xz', function(){
            var h = new Helper([10, 15]);
			//console.log(h.SizeMap);
			expect(h.SizeMap[0]).toBe(10);
			expect(h.SizeMap[1]).toBe(15);
		});
		
		it('should be have #CoordInRightTriangle', function(){
            Helper.should.be.have.property('CoordInRightTriangle');
			Helper.CoordInRightTriangle.should.be.a('function');
		});
})