﻿var Model = function(){
    //Общие функции
    var getFunction = function(){
        if(this.hasOwnProperty('_' + name))
            return this['_' + name];
        else
            throw new Error("GObject don't have property: " + name);
    };
    var setFunction = function (name, value) {
        if(arguments.length < 2)
            throw new Error("setter must have 2 parameters!");

        if(arguments.length == 3) //третий - он же первый - доп функция
        {
            name = arguments[1];
            value = arguments[2];
        }

        if(this.hasOwnProperty('_' + name))
        {
            if(this['_' + name] != value)
            {
                this['_' + name] = value;

                if(typeof arguments[0] == 'function')
                    arguments[0](name, value); //вызов передаваемой(с помощью bind) функции
            }
        }
        else
            throw new Error("GObject don't have property: " + name);
    };

	//Игровой объект (самостоятельный или часть других объектов)
	this.GObject = {
        changeXY: false, //изменения в передвижении?
        changeState: false, //изменение состояния объекта?

        //private
        _health: 100, //здоровье
        _armor: 0, //броня
		_x: 0, //начало координат - верхний левый угол
		_y: 0,
		_rotateAngle: 0, //угол поворота
        _speed: 10, //скорость
        _force: 0, //сила
        _aimXY: [], //Место прицеливания
        _abilities: [], //способности
        _activeAbility: null, //активная возможность
        _name: '', //название
        _type: '', //тип объекта
        _effects: [], //наложенные эффекты
        _squareCoordinates: [], //координаты квадрата местоположения
        _linkObjects: [], //Связанные объекты, на них передаётся эффект перемещения
        _pointCreateWeaponXY: null, //Место привязки текущего оружия
        _deleted: false, //пора удалять?
        _img: null, //объект с данными о картинках
        _weapon: null, //оружие

        get: getFunction.bind(this),
        set: setFunction.bind(this, function () {
            this.changeState = true; //произошло изменение состояния объекта

            if (name == 'x' || name == 'y')
                this.changeXY = true; //изменения в местоположении объекта
        }),
        AddAbilities: function(name)
        {
        },
        AddEffect: function(name, args){
            if(this._effects[name])
            {
                for(var i in args) //все аргументы
                    if(args.hasOwnProperty(i)) //это действительно элемент массива, а не его свойство
                    {
                        if (typeof args[i] == "number") //если число
                            this._effects[name][i] += args[i]; //складываем
                        else if (typeof args[i] == "object") //массив или сам объект
                        {
                            for (var z in args[i]) //каждый эл массива или объекта
                                if(args[i].hasOwnProperty(z))
                                    this._effects[name][i].push(args[i][z]);
                        }
                        else
                            this._effects[name][i] = args[i]; //заменяем
                    }
            }
            else
                this._effects[name] = args;
        },
        AddLinkObject: function(gObject, arrayLinkEffects, args){
            this._linkObjects.push({
                el: gObject,
                arrayLinkEffects: arrayLinkEffects,
                args: args
            });
        }
	};

    //Игрок
    this.Gamer = {
        name: '',
        nikName: '',

        mapKeys: [], //все нажатые клавиши
        rise: '', //расса
        personage: '', //персонаж (выбранный игроком перед игрой)

        GObject: null
    };

    //Изображения
    this.Img = {
        images: [], //картинки
        countFrames: 1, //количество кадров
        extensionFile: 'jpg', //jpg, png
        randomStart: false, // рандомный первый кадр?
        zIndex: 1, //приоритет прорисовки(чем выше тем выше элемент)
        path: '', //путь до папки с картинками

        //private
        _currentFrame: 1, //номер показываемого кадра

        get: getFunction.bind(this),
        set: setFunction.bind(this, function (name, value) {
            if(name == 'currentFrame')
            {
                if (value < 1)
                    this._currentFrame = 1;
                else if (value > this.img.countFrames)
                    this._currentFrame = this.img.countFrames;
            }

            if (this.gObject) //если есть физ сущность
                this.gObject.changeState = true; //передаем туда событие
        }),

        Animation: null, //анимация
        GObject: null //физ сущность
    };

    //Анимация (часть Img)
    this.Animate = {
        timeChangeFrame: 100, //время между кадрами анимации
        revers: false //в обратном направлении показывать?
        //timeLife: 3000 //время жизни всей анимации //TODO: время жизни в чём измерять?
    };

    //Музыка
    this.Sound = {
        radius: 10, //радиус слышимости
        volume: 100 //мощность в процентах от нального
    };

	//Оружие
	this.Weapon = {
        name: '', //название
		rateOf: 100, //скорострельность (миллисекунд между выстрелами)
		TimeRateOf: 0, //время последней атаки
		damage: 1, //урон
		radiusDamage: 1, //радиус урона
		speedShot: 10, //скорость выстрела (кол-во пикселей в секунду)
		volumeHolder: 10, //объём обоймы
		timeReloading: 1000, //время перезарядки (в миллисекундах)
		typeScatter: null, //тип разброса
		scatterMax: 1, //максимальный разброс
		scatter: 0,  //разброс
		pointCreateBulletXY: null, //точка создания боеприпаса

        GObject: null, //физ сущность
        Bullet: null, //Боеприпас
		ImgGamer: null, //изображение с видом сверху в руках у игрока
		ImgShop: null, //изображение в профиль для магазина
		ImgShot: null, //Анимация выстрела
		SoundShot: null //Звук выстрела
	};

	//Снаряд/пуля (часть Weapon)
	this.Bullet = {
		name: '', //название
		damage: 1, //урон самой пули (складывается с уроном оружия)
		damageArmor: 0, //бронебойность
		radiusDamage: 1, //радиус урона
		distance: 1000, //дальность полёта
		ids_Contact: [], //id просмотренных объектов (если пуля пробьёт цель и останется в строю, то это поможет вылететь пули из объекта и сэкономит кучу времени на проверки)

        GObject: null,
		ImagesContact: null, //картинки столкновений [тип материала] => картинка

        //private
		_pointRay: [], //массив лучей траектории объекта

		getPointRay: function(value, n){
			if(value === undefined && n === undefined)
				return this._pointRay;
			else if(value === undefined)
				return this._pointRay[n];
			
			this._pointRay.push({p1: value, p2: [value[0] + this.dx, value[1] + this.dy]});
			var ray = this._pointRay[this._pointRay.length - 1]; //для краткости
			ray.a = ray.p2[0] - ray.p1[0];
			ray.b = ray.p1[0] * ray.p2[1] - ray.p2[0] * ray.p1[1];
			ray.z = ray.p1[1] - ray.p2[1];
		},
		
		//Функции прямой
		Fx: function(y, n){ //n - номер луча
			return (-this._pointRay[n].a * y - this._pointRay[n].b) / this._pointRay[n].z;
		},
		Fy: function(x, n){
			return (-this._pointRay[n].z * x - this._pointRay[n].b) / this._pointRay[n].a;
		}
	};

};

module.exports = Model; //делаем файл модулем