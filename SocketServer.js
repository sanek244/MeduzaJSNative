﻿var SocketServer = function (port, Controller){
	//Подключаем сокеты и ставим на прослушивание порта
	var io = require('socket.io').listen(port); 
	var controller = new Controller("testings_polygon");
	var users = {};
	var timeReturn = 1 * 10; //Время на возврат в игру после потери соединения //TODO: вернуть к 3-м минутам
	setInterval(Ping, 1000); //пилинг всех каждую секунду
	setInterval(SendDataAll, 8); //отправка данных игрокам (125 fps)

	//Событие подключения нового клиента
	io.sockets.on('connection', function(socket){
		socket.on('ping', function(msg){ //ответ клиента на пилинг
			if(users[socket.id] && msg)
				users[socket.id].lastTimeAction = new Date().getTime();
		});
		
		socket.on('hash', function(msg){ //подключившийся клиент прислал хэш код из кук
			//TODO: Добавить проверку на наличие ключя доступа к серверу и его правильность

			if(msg == "" || (typeof msg == "string" && msg.length == 32 && !ContainsValueAttr(users, "id", msg))) //если его нету или он нормальной длины, но нету в массиве пользователей
			{
				var hash = GenerateHasCod(32); //создаём новый хеш код
				users[socket.id] = {'id': hash, 'lastTimeAction': new Date().getTime()}; //добавляем пользователя в массив
				controller.NewGamer(hash, "warrior");
				socket.json.emit('loadCollection', controller.GetCollectionAll()); //отправка игроку всех необходимых данных(картинок, звуков)
				socket.json.emit('hash', hash); //отправляем пользователю его хеш код
				console.log(date() + "connect - " + hash);
				socket.json.emit('view', controller.GetView(users[socket.id].id, true));
			}
			else if(typeof msg == "string" && msg.length == 32)
			{			
				var idSocket = ContainsValueAttr(users, "id", msg);
				if(idSocket && users[idSocket]['lastTimeAction'] + timeReturn * 1000 > new Date().getTime())
				{
					socket.json.emit('errors', 'Ошибка! Возможно вы открыли игру во второй вкладке браузера! Разрешено играть только в 1 вкладке!');
					console.log(date() + "Message! multiplay: " + msg);
				}
				else
				{
					users[socket.id] = users[idSocket];
					delete users[idSocket];
					users[socket.id]["lastTimeAction"] = new Date().getTime();
					console.log(date() + "reconnect - " + users[socket.id].id);
					socket.json.emit('loadCollection', controller.GetCollectionAll()); //отправка игроку всех необходимых данных(картинок, звуков)
					socket.json.emit('view', controller.GetView(users[socket.id].id, true)); //отправка кртинки
				}
			}
			else
			{
				socket.json.emit('errors', 'Некорретные данные(хеш код) Очистите сookie и попробуйте снова!');
				console.log(date() + "error! invalid hash: " + msg);
				socket.disconnect();
			}
		});
		
		socket.on('keyControl', function(msg){ //Нажатие или отпускание клавиши
			if(users[socket.id])
				if(typeof msg != "string" || msg.length > 11 || msg.indexOf(':') == -1 || msg.split(':')[1].length > 3 || parseInt(msg.split(':')[1]) > 1000 || !(parseInt(msg.split(':')[1]) > 0)) //максимум код клавиши меньше 1000, всё что больше - мусор и спам или глюки.
				{
					socket.json.emit('errors', 'Некорретные данные(код клавиши)');
					delete users[socket.id];
					socket.disconnect();
					console.log(date() + "error! invalid keyControl: " + msg);
				}
				else
				{
					users[socket.id].lastTimeAction = new Date().getTime();
					controller.Key(users[socket.id].id,  msg.split(':')[0],  msg.split(':')[1]);
				}
		});
		
		socket.on('click', function(msg){ //Нажатие или отпускание клавиши мыши от клиента
			if(users[socket.id])
			{
				if(msg.length > 23 || msg.indexOf(':') == -1) //максимум разрешение экрана 5к по всем осям => 4цифры + 3 знака разделителя + 4 цифры + 3 цифры + 10 букв = 23 знака, всё что больше - мусор и спам или глюки.
				{
					socket.json.emit('errors', 'Некорретные данные(координаты мыши)');
					delete users[socket.id];
					socket.disconnect();
					console.log(date() + "error! invalid click: " + msg);
				}
				else
				{
					users[socket.id].lastTimeAction = new Date().getTime();
					controller.Click(users[socket.id].id, msg.split(':'));
				}
			}
		});
	
		socket.on('resize', function(){ //Изменение размера окна
			if(users[socket.id])
			{
				controller.ClearKeyMap(users[socket.id].id);
				io.sockets.json.emit('view', controller.GetView(users[socket.id].id, true));
			}
		});
	
		socket.on('mousemove', function(msg){ //Перемешение мыши
			if(users[socket.id])
			{
				if(msg.length > 9 || msg.indexOf(':') == -1) //максимум разрешение экрана 5к по всем осям => 4цифры + 1 знак разделитель + 4 цифры = 9 знаков, всё что больше - мусор и спам или глюки.
				{
					socket.json.emit('errors', 'Некорретные данные(координаты мыши при перемещении)');
					delete users[socket.id];
					socket.disconnect();
					console.log(date() + "error! invalid mousemove: " + msg);
				}
				else
				{
					users[socket.id].lastTimeAction = new Date().getTime();
					controller.MouseMove(users[socket.id].id, msg.split(':'));
				}
			}
		});
	});

	/*	Вспомогательный функционал */
	function SendDataAll(){ //Отправка данных игрокам
		//Проверка на изменений коллекции объектов
		var CollectionChange = controller.GetCollection();
		if(CollectionChange)
			io.sockets.json.emit('changeCollection', CollectionChange);
		
		for(var idUser in users)
		{
			//Визуальное представление
			if(data = controller.GetView(users[idUser].id))
				io.sockets.json.emit('view', data);
			
			//звуки
			if(data = controller.GetCollectionAddSound(users[idUser].id))
				io.sockets.json.emit('sound', data);
			
			//отдача
			if((data = controller.GetScatter(users[idUser].id)) !== false)
				io.sockets.connected[idUser].json.emit('scatter', data);
		}
		controller.ClearChange(); //очистка изменений
	}
	
	function GenerateHasCod(length){ //Генерация хеш кода
		var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		var code = "";
		var clen = chars.length - 1;  

		while (code.length < length) 
			code += chars[rand(0, clen)];  

		return code;
	}

	function ContainsValueAttr(array, attr, value){ //Содержится ли в массиве значение указанного атрибута
		for(var index in users) 
			if(users[index][attr] == value)
				return index;
		
		return false;
	}

	function Ping(){ //Пилингование всех пользователей
		
		for(var index in users)
		{
			if(users[index]['lastTimeAction'] > timeReturn) //если не помечен, как вылетивший
			{
				if(users[index]['lastTimeAction'] + 5000 < new Date().getTime()) // через 5 сек молчания оповещаем всех
					io.sockets.json.emit('disconect', index);
				
				if(users[index]['lastTimeAction'] + 1000 < new Date().getTime())
					if(io.sockets.connected[index])
						io.sockets.connected[index].json.emit('ping', true); //пилингуем
					else //если соединения нет
					{
						console.log(date() + "off: " + users[index].id); //log
						io.sockets.json.emit('disconect', index); //оповещаем всех 
						users[index]['lastTimeAction'] = timeReturn; //даём 3 минуты на возврат
					}
			}
			else if(users[index]['lastTimeAction'] == 0) //не успел вернуться
			{
				controller.DeleteGamer(users[index].id);
				delete users[index];
			}
			else //3 минуты на возврат
				users[index]['lastTimeAction']--;
		}
	}

	function rand(min, max){ //Округлённый рандом 
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function date(){ //Текущая дата в особом формате
		var t = new Date();
		return /*t.getFullYear() + "-" + t.getMonth() + "-" + */t.getDate() + " " + t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds() + " ";
	}
}

module.exports = SocketServer; //делаем файл модулем