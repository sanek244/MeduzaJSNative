﻿var GameController = function(nameMap){
	//***	Переменные    ***//
	var width = 1920, height = 1200; //размеры игрового окна
	var SizeMap = [10000, 5000]; //Размер игровой карты (width, height)
	
	var Helper = require('./../Helper'); Helper = new Helper(SizeMap); //Вспомогательный функционал
	var Model = require('./../Model'); Model = new Model(Helper); //модель данных
	var Logic = require('./../Logic'); Logic = new Logic(Helper); //Логика некоторых элементов
	var FileManager = require('./../Managers/FileManager'); Logic = new FileManager(); //Логика некоторых элементов
	var extend = require('util')._extend;
	
	var gamers = new Array(); //массив игроков
	var GObjects = new Array(); //массив динамических объектов(пули) + тела игроков
	var CoordGobjects = {}; //Ассоциативный массив вида: номер квадрата=>[GObject, ...]
	
	var CollectionChange = {}; //Изменения в коллекции объектов
	var CollectionAddSound = []; //пул звуков, которые надо запустить у клиентов
	var viewChange = {}; //изменения визуальной части (что бы каждый раз не слать всю картинку) номер квадрата=>[GObject, ...]
	var MessageKills = {}; //Сообщения об убийствах игроков [name кто убил] = name кого убили
	
	var idNewGobject = 0; //id нового объекта в массиве GObjects
	var TimeComplite = new Date().getTime(); //время прошедшее между выполнением кода
	
	setInterval(Move, 8, this);
	
	var Storage = {img: FileManager.LoadImages(), Sound: FileManager.LoadSounds()};  //массив с данными из БД
	Storage.Bulet = FileManager.LoadBulets(Storage); //тип боеприпасов
	Storage.Weapon = FileManager.LoadWeapons(Storage); //типы оружия
	Storage.Gamer = HelpFileManagerer.LoadGamers(Storage); //типы игроков
	Storage.Gobjects = FileManager.LoadGObject(Storage); //игровые объекты
					
	//** Свойства **//
	//Добавление нового игрока с никнеймом
	this.NewGamer = function(id, type, nikname){
		gamers[id] = new Model.Gamer(nikname, Helper.Clone(Storage.Gamer[type]), Storage.Gamer[type].rise); //данные объекта игрока;
		
		if(gamers[id].gobject.rise == "human")
			for(var i = 1; i < 3; i++)
				if(GObjects["startHuman" + i])
				{
					gamers[id].gobject.X(GObjects["startHuman" + i].X());
					gamers[id].gobject.Y(GObjects["startHuman" + i].Y()); //место появления
					gamers[id].gobject.RotateAngle(GObjects["startHuman" + i].RotateAngle());
					this.DeleteGObject("startHuman" + i);
					break;
				}
		
		this.AddGObject(gamers[id].gobject, id);  //добавляем игрока в массив
		
		//Анимация ног
		var bottomGamer = new Model.Animate(gamers[id].gobject.imgRun, gamers[id].gobject.X(), gamers[id].gobject.Y(), -90, 0, 0, false);
		this.AddGObject(bottomGamer, id + "_bottom");
		gamers[id].gobject.bottom = bottomGamer; //как свойство для управления поворотом
		gamers[id].gobject.AddLinkObject(bottomGamer, ["MovingKeys"]); //в зависимые объекты
		
		//если есть оружие
		if(gamers[id].gobject.Abilitys[0].weapon)
		{
			var weapon = gamers[id].gobject.Abilitys[0].weapon;
			weapon.id = id + "_weapon_";
			weapon.X(gamers[id].gobject.X() + gamers[id].gobject.pointCreateWeaponXY[0] + weapon.img.size[0] / 2);
			weapon.Y(gamers[id].gobject.Y() + gamers[id].gobject.pointCreateWeaponXY[1] + weapon.img.size[1] / 2);
			weapon.RotateAngle(gamers[id].gobject.RotateAngle());
			weapon.physics = false;
			gamers[id].gobject.AddLinkObject(weapon, ["MovingKeys", "Rotate"], {dx: gamers[id].gobject.pointCreateWeaponXY[0] + weapon.img.size[0] / 2, dy: gamers[id].gobject.pointCreateWeaponXY[1] + weapon.img.size[1] / 2});
			this.AddGObject(weapon, id + "_weapon");
		}
	};
	
	//Добавление объекта
	this.AddGObject = function(GObject, id){
		if(!id)
			id = idNewGobject++;
		
		GObject.effects = [];
		GObject.linkObjects = [];
		GObject.id = id;
		GObjects[id] = GObject;
		GObjects[id].lastCoordQuard = [-1];
		if(GObjects[id].img)
		{
			GObjects[id].view = Helper.generateView(GObjects[id]); //формирование изображения объекта
			Helper.SetGObjectsInQuars(CoordGobjects, GObjects[id]) //расчёт и занесение местоположения
			Helper.SetGobjectsInChangeQuards(viewChange, GObjects[id]); //запись изменений

			CollectionChange[id] = {img: GObject.img};//изменена коллекция
		}
	}
	
	//Добавление звуков в пул 
	this.AddSound = function(x, y, sound){
		CollectionAddSound.push({x: x, y: y, sound: sound});
	}
	
	//Удаление объекта
	this.DeleteGObject = function(id){
		if(GObjects[id])
		{
			for(var c in GObjects[id].coordQuard) //из координат
			{
				delete CoordGobjects[GObjects[id].coordQuard[c]][id];
				if(Object.keys(CoordGobjects[GObjects[id].coordQuard[c]]).length == 0)
					delete CoordGobjects[GObjects[id].coordQuard[c]];
			}
			//удаление связанных объектов
			for(var i in GObjects[id].linkObjects)
				this.DeleteGObject(GObjects[id].linkObjects[i].id);
			
			delete GObjects[id];
			CollectionChange[id] = "deleted";
		}
	}
	
	//Удаление игрока
	this.DeleteGamer = function(id){
		if(gamers[id]) //если существует
		{
			delete gamers[id];
			this.DeleteGObject(id);
			this.DeleteGObject(id + "_scatter");
			this.DeleteGObject(id + "_bottom");
			this.DeleteGObject(id + "_weapon");
		}
	}
	
	//Очистка карты нажатых клавиш
	this.ClearKeyMap = function(id){ 
		gamers[id].mapKeys = [];
	}
	
	//Очистка изменений
	this.ClearChange = function(){
		viewChange = {};
		CollectionAddSound = [];
	}
	
	
	//** События **//
	//Нажатие клавиши игроком
	this.Key = function(id, type, key){
		gamers[id].mapKeys[key] = type == 'keydown';
	}
	
	//Нажатие клавиши мыши игроком
	this.Click = function(id, XY){ 
		gamers[id].mapKeys[XY[3]] = XY[2] == 'mousedown';
		gamers[id].gobject.AimXY([XY[0], XY[1]]);
	}
	
	//Перемещение мыши
	this.MouseMove = function(id, ArrayXY){
		gamers[id].gobject.AimXY([parseInt(ArrayXY[0]), parseInt(ArrayXY[1])]);
		gamers[id].gobject.ChangeRotate = true;
	}
	
	
	//** Get-теры **//
	//Получение нового визуального представления для игрка(Возвращает пустоту, если представление не изменилось!!!)
	this.GetView = function(idUser, allView){ //allView - Вернуть всё в зоне видимости
		return cutView(idUser, allView);
	}
	
	//Формирование ассоциативного массива [id object] = [Img]
	this.GetCollectionAll = function(){
		var StorageTemp = {};
		
		for(var i in GObjects)
			StorageTemp[i] = {img: GObjects[i].img};
		
		return StorageTemp;
	}
	
	//Возврат ассоциативного массива
	this.GetCollection = function(){
		var res = CollectionChange;
		
		if(Object.keys(CollectionChange).length === 0)
			return false;
		
		CollectionChange = {};
		return res;
	}
	
	//Возвращает звуки слышимые данному игроку
	this.GetCollectionAddSound = function(idUser){
		var res = [];
		for(var i in CollectionAddSound)
		{
			if(CollectionAddSound[i].sound.radius == 0) //музыка без радиуса действия
				res.push({nameSound: CollectionAddSound[i].sound.nameSound, volume: CollectionAddSound[i].sound.volume});
			else //расчёт вхождения в зону действия и громкости
			{
				var distance = Math.sqrt(Math.pow(gamers[idUser].gobject.X() - CollectionAddSound[i].x, 2) + Math.pow(gamers[idUser].gobject.Y() - CollectionAddSound[i].y, 2))
				if(distance < CollectionAddSound[i].sound.radius)
					res.push({nameSound: CollectionAddSound[i].sound.nameSound, volume: ((CollectionAddSound[i].sound.radius - distance) / CollectionAddSound[i].sound.radius) * CollectionAddSound[i].sound.volume});
			}
		}
		if(res.length == 0)
			return false;
		
		return res;
	}

	//Возвращает отдачу оружия игрока
	this.GetScatter = function(idUser){
		if(gamers[idUser].gobject.effects["Scatter"]) //если есть разброс
		{
			if(gamers[idUser].gobject.effects["Scatter"].scatter > 0)
				return gamers[idUser].gobject.effects["Scatter"].scatter;
			if(gamers[idUser].gobject.effects["Scatter"].scatterOld > 0)
				return 0;
		}
		
		return false;
	}
	
	
	//*** Значимый функционал ***//
	//Загрузка карты
	this.LoadMap = function(nameMap){
		var fs = require('fs');
		var text = fs.readFileSync('db/maps/' + nameMap + '.txt', 'utf8').split('\r\n');
		
		for(var t in text)
		{
			var el = text[t].split(' ');
			if(el[0] != "#")
			{
				var obj = Helper.Clone(Storage.Gobjects[el[0]]);
				obj.X(parseInt(el[1]));
				obj.Y(parseInt(el[2]));
				obj.RotateAngle(parseInt(el[3]) || 0);
				this.AddGObject(obj, el[4]);
			}
		}
	}
	
	//***	Закрытые методы  ***//
	//Приминение кликов
	function ClickApply(id, GameController){
		if(gamers[id].gobject.Abilitys && gamers[id].mapKeys[0]) //XY[2] = 0(левая) || 2(правая) кнопка
			gamers[id].gobject.ActiveAbility().Click(GameController, gamers[id].gobject);
	}
	
	//Жизнь объектов
	function Move(GameController){
		TimeComplite = new Date().getTime() - TimeComplite; //время прошедшее между прорисовками
	
		for(var id in gamers) //Приминение нажатых клавиш
		{
			if(gamers[id].mapKeys[0] || gamers[id].mapKeys[2]) //нажата клавиша мыши
				ClickApply(id, GameController);
			
			if(Helper.DownMoveKeys(gamers[id].mapKeys)) //Перемещение
			{
				if(gamers[id].gobject.bottom)
					gamers[id].gobject.bottom.animate = true;
				
				for(var key in gamers[id].mapKeys) 
					if(gamers[id].mapKeys[key])
						gamers[id].gobject.AddEffect("MovingKeys", {keys: [key]});
				gamers[id].gobject.AddEffect("Rotate", {angle: Helper.RotateAngleObject(gamers[id].gobject)}); //поворот
			}
			else if(gamers[id].gobject.ChangeRotate) //поворот
			{
				gamers[id].gobject.AddEffect("Rotate", {angle: Helper.RotateAngleObject(gamers[id].gobject)}); //поворот
				gamers[id].gobject.ChangeRotate = false;
			}
		}
		
		//объекты
		for(var i in GObjects)
			if(GObjects[i].img)
			{
				GObjects[i].lastCoordQuard = Helper.Clone(GObjects[i].coordQuard); //старое местоположение
				
				for(var e in GObjects[i].effects) //эффекты
				{
					Logic.Effects[e](GObjects[i], GObjects[i].effects[e], CoordGobjects, TimeComplite, GObjects); //обработка эффектов
					if(e == "Score" && GObjects[i].effects[e].args.kill && GObjects[effects[e].args.kill].rise) //кого-то убили
					{
						if(gamers[i])
							GameController.MessageKills.push({killer: gamers[i].name, kill: gamers[effects[e].args.kill].name, killerRise: gamers[i].rise, killRise: gamers[effects[e].args.kill].rise});
						else
							GameController.MessageKills.push({killer: GObjects[i].type, kill: gamers[effects[e].args.kill].name, killRise: gamers[effects[e].args.kill].rise});
					}
				}
					
				if(GObjects[i].Move)
					GObjects[i].Move(TimeComplite, CoordGobjects, GameController);
				
				if(GObjects[i].changeXY) //были изменения в движении
				{
					Helper.SetGObjectsInQuars(CoordGobjects, GObjects[i]); //расчёт и запись квадрата местоположения
					
					if(!GObjects[i].changeState) //изменений нет, но в предыдущий раз было движение
					{
						GObjects[i].changeXY = false;
						GObjects[i].changeState = true; //запись изменений
						GObjects[i].animate = false;
					}
				}
					
				if(GObjects[i].changeState) //если были изменения
				{
					if(GObjects[i].Delet()) //если пора удалять объект
					{
						GameController.DeleteGObject(i);
					}
					else
					{
						GObjects[i].view = Helper.generateView(GObjects[i]); //перерисовываем
						Helper.SetGobjectsInChangeQuards(viewChange, GObjects[i]); //запись изменений
						GObjects[i].changeState = false; //сбиваем изменения
					}
				}
			}
		
		TimeComplite = new Date().getTime();
	}
	
	//Обрезка изображения 
	function cutView(idUser, allView){
		/*Обрезка 
			ширина условно 1920 пикселей, высота 1200; 
			камера направлена на игрока. */
		var window = {x: gamers[idUser].gobject.X() - width / 2, width: width, height: height, y: gamers[idUser].gobject.Y() - height / 2};

		var areaView = viewChange;
		
		var mas = gamers[idUser].gobject.coordQuard;
		var masOld = gamers[idUser].gobject.lastCoordQuard;
		if(allView || masOld && (mas.length != masOld.length || mas[0] != masOld[0] || mas[mas.length - 1] != masOld[mas.length - 1])) //все что видно
			areaView = CoordGobjects;
		
		var arr = Helper.GetGobjectsInQuards([Helper.FormulaLocation(window.x, window.y), Helper.FormulaLocation(window.x + window.width, window.y), Helper.FormulaLocation(window.x, window.y + window.height), Helper.FormulaLocation(window.x + window.width, window.y + window.height)],
			areaView, true); //берём объекты в квадрате видимости
		
		if(Object.keys(arr).length === 0)
			return false;

		return arr; //обрезанное визуальное представление
	}

	
	this.LoadMap(nameMap); //загрузка карты
}

module.exports = GameController; //делаем файл модулем