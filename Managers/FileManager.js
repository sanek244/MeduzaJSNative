var FileManager = function() {
    var Helper = require('../Helper'); //вспомогательный функционал
    var Model = require('../Model'); //модели
    var FS = require('fs');
    var SizeOfImg = require('../lib/image-size-master/lib/index'); //функция определения размера картинки

    this.Images = null; //картинки
    this.Sounds = null; //музыка
    this.Bullets = null; //типы боеприпасов
    this.Weapons = null; //типы оружий
    this.Gamers = null; //типы игроков
    this.Gobjects = null; //объекты

    //Размер картинки
    var sizeImg = function(path){
        var dimensions = SizeOfImg("public_html\\img\\" + path);
        return [dimensions.width, dimensions.height];
    };

    //Загрузка чего-либо из файлов
    var Load = function (fileName, functionHandler) {
        var strings = FS.readFileSync('db/' + fileName + '.txt', 'utf8').split('\r\n'); //строчки текста из файла

        var arrayRes = []; //результат
        for(var i in strings) //все строки
            if(strings.hasOwnProperty(i))
            {
                var dataString = strings[i].split(' '); //данные строки
                if(dataString[0] != "#") //не комментарий
                    arrayRes[dataString[0]] = functionHandler(dataString);
            }

        return arrayRes;
    };

    //Загрузка картинок
    this.LoadImages = function(){
        this.Images = Load('img', function(dataString){
            var res = Helper.Clone(Model.Img);
            res.name = dataString[0];
            res.path = dataString[1];
            res.countFrames = dataString[2];
            if(dataString[3] > 0) { //время смены кадра
                //анимация
                res.Animate = Helper.Clone(Model.Animate);
                res.Animate.timeChangeFrame = dataString[3];
            }
            res.extensionFile = dataString[4];
            res.randomStart = dataString[5];
            res.zIndex = dataString[6];
            res.size = sizeImg(dataString[1] + "\\" + dataString[0] + "_1." + dataString[4]);

            return res;
        });
        return this.Images;
    };

    //Загрузка звуков
    this.LoadSounds = function(){
        this.Sounds = Load('sound', function(dataString){
            var res = Helper.Clone(Model.Sound);
            res.name = dataString[0];
            res.radius = parseInt(dataString[1]);
            res.volume = parseInt(dataString[2]);

            return res;
        });
        return this.Sounds;
    };

    //загрузка типов игроков
    this.LoadGamers = function(){
        this.Gamers = Load('typeGamer', function(dataString){
            var res = Helper.Clone(Model.Gamer);
            res.name = dataString[0];
            res.GObject = Helper.Clone(Model.GObject);
            res.GObject._health = parseInt(dataString[1]);
            res.GObject._armor = parseInt(dataString[2]);
            res.GObject._force = parseInt(dataString[3]);
            res.GObject._speed = parseInt(dataString[4]);
            res.personage = dataString[5];
            res.rise = dataString[6];
            res.GObject._pointCreateWeaponXY = {
                x: parseInt(dataString[7]),
                y: parseInt(dataString[8])
            };
            if(!this.Weapons) //оружия ещё не загружены
                this.LoadWeapons();
            res.GObject.weapon = Helper.Clone(this.Weapons[dataString[9]]);
            res.GObject.AddAbilities("Strike");
new Logic.Abilitys["Strike"](this.Clone(Storage.Weapon[dataString[9]]), [parseInt(dataString[7]), parseInt(dataString[8])], 0, "bio");
            arrayRes[dataString[0]].rise = dataString[6]; //Расса
            if(Storage.img[dataString[0] + "_bottom"])
                arrayRes[dataString[0]].imgRun = Storage.img[dataString[0] + "_bottom"]; //ноги
            if(Storage.img[dataString[0] + "_scatter"])
                arrayRes[dataString[0]].imgScatter = Storage.img[dataString[0] + "_scatter"]; //отдача


            return res;
        });
        return this.Gamers;
    };

    //Загрузка боеприпасов
    this.LoadBullets = function(Storage){
        var strings = FS.readFileSync('db/typeBulet.txt', 'utf8').split('\r\n');

        var arrayRes = [];
        for(var i in strings)
            if(strings.hasOwnProperty(i))
            {
                var dataString = strings[i].split(' ');
                if(dataString[0] != "#") //не комментарий
                {
                    var imgsContact = {};
                    if(dataString[6]) //typeContactObjects - тип контакта с объектами
                    {
                        imgsContact["bio"] = Storage.img[dataString[6] + "Contact_bio"]; //био
                        imgsContact["metal"] = Storage.img[dataString[6] + "Contact_metal"]; //металл
                        imgsContact["concrete"] = Storage.img[dataString[6] + "Contact_concrete"]; //бетон
                        imgsContact["tree"] = Storage.img[dataString[6] + "Contact_tree"]; //дерево
                    }
                    arrayRes[dataString[0]] = Helper.Clone(Model.Bulet(dataString[0], Storage.img[dataString[0]], parseInt(dataString[1]), parseInt(dataString[2]), parseInt(dataString[3]), parseInt(dataString[4]), parseInt(dataString[5]), "metal", imgsContact));
                }
            }

        return arrayRes;
    };

    //Загрузка оружия
    this.LoadWeapons = function(Storage){
        var strings = FS.readFileSync('db/typeWeapon.txt', 'utf8').split('\r\n');

        var arrayRes = [];
        for(var i in strings)
            if(strings.hasOwnProperty(i))
            {
                var dataString = strings[i].split(' ');
                if(dataString[0] != "#") //не комментарий
                    arrayRes[dataString[0]] = Helper.Clone(Model.Weapon(dataString[0], Storage.img[dataString[0] + "_top"], Storage.img[dataString[0]], parseInt(dataString[1]), parseInt(dataString[2]), parseInt(dataString[3]), parseInt(dataString[4]),
                        parseInt(dataString[5]), parseInt(dataString[6]), parseFloat(dataString[7]), dataString[8], Storage.Bulet[dataString[9]], [parseInt(dataString[10]), parseInt(dataString[11])], Storage.Sound[dataString[12]], Storage.img[dataString[13]]));
            }
        return arrayRes;
    };

    //Загрузка игровых объектов
    this.LoadGObject = function(Storage){
        var strings = FS.readFileSync('db/GObjects.txt', 'utf8').split('\r\n');

        var arrayRes = [];
        for(var i in strings)
            if(strings.hasOwnProperty(i))
            {
                var dataString = strings[i].split(' ');
                if(dataString[0] != "#") //не комментарий
                {
                    arrayRes[dataString[0]] = Helper.Clone(Model.GObject(dataString[0], dataString[2] ? Storage.img[dataString[2]] : "", 0, 0, parseInt(dataString[3]) || 0, parseInt(dataString[4]) || 0, parseInt(dataString[5]) || 0, parseInt(dataString[6]) || 0, dataString[7] || "none"));
                    arrayRes[dataString[0]].animate = dataString[9] == 'true' ? true : false;
                    arrayRes[dataString[0]].physics = dataString[1] == 'true' ? true : false;
                    arrayRes[dataString[0]].typeStructure = dataString[8] || "";
                }
            }
        return arrayRes;
    };
};

module.exports = FileManager; //делаем файл модулем