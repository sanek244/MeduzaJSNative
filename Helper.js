﻿var Helper = function(SizeMap){
	var Logic = require('./Logic');
    Logic = new Logic(this); //модель данных

	this.SizeMap = SizeMap; //Размер карты 
	this.sizeQuard = 200; //Размер сетки
	this.SizeMapQuards = 0; //Размер карты в квадратах
	
	//Вычисление координат в прямоугольном треугольнике
	this.CoordInRightTriangle = function(xAngle, yAngle, rotateAngle, length){ //length - длина гипотенузы 
		return [xAngle + Math.cos(rotateAngle * Math.PI / 180) * length, yAngle + Math.sin(rotateAngle * Math.PI / 180) * length];
	}
	
	//Расстояние между точками
	this.Distance = function(x1, y1, x2, y2){
		if(isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2)) //ошибочные входные данные
		{
			//console.log("NaN in Helper.Distance");
			return 99999999999; //заведомо большое число
		}
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
	}
	
	//Поворот объекта
	this.RotateAngle = function(x, y, AimXY){ //AimXY - местоположение курсора
		var X = x - AimXY[0];
		return (Math.atan((y - AimXY[1]) / X) + (X >= 0 ? Math.PI : 0)) * 180 / Math.PI;
	}
	this.RotateAngleObject = function(gobject){
		var X = gobject.X() - gobject.AimXY()[0];
		return (Math.atan((gobject.Y() - gobject.AimXY()[1]) / X) + (X >= 0 ? Math.PI : 0)) * 180 / Math.PI;
	}
	
	//Координаты полученные путём вращения прямой
	this.CoordRotate = function(xAngle, yAngle, xAim, yAim, newAngle){
		return this.CoordInRightTriangle(xAngle, yAngle, newAngle, this.Distance(xAngle, yAngle, xAim, yAim));
	}
	
	//Расчёт псевдо хэша строки
	this.hashStr = function(str){
		var sum = 0;
		
		for(var i = 0; i < str.length; i++)
			sum += str.charCodeAt(i) * Math.pow(72, i / 1000);
			
		return (sum + " ").substring(0, 17);
	}

	//нажата движующая клавиша ?
	this.DownMoveKeys = function(mapKeys){ 
		return mapKeys[38] || mapKeys[87] || mapKeys[39] || mapKeys[68] || mapKeys[40] || mapKeys[83] || mapKeys[37] || mapKeys[65];
	}

	//Вычислить координаты точки при повороте вокруг центра на удалении r
	this.Coord = function(xCenter, yCenter, x, y, rotateAngle){
		rotateAngle = (rotateAngle + this.RotateAngle(xCenter, yCenter, [x, y])) * Math.PI / 180;
		var r = Math.sqrt(Math.pow(xCenter - x, 2) + Math.pow(yCenter - y, 2));
		
		if(rotateAngle == 0)
			return [x, y];
		else if(rotateAngle == 90)
			return [xCenter, yCenter - r];
		else if(rotateAngle == 180)
			return [xCenter - 2 * r, y];
		else if(rotateAngle == 270)
			return [xCenter, yCenter + r];
		else if(rotateAngle < 90)
			return [xCenter + Math.cos(rotateAngle) * r, yCenter + Math.sin(rotateAngle) * r];
		else if(rotateAngle < 180)
			return [xCenter + Math.sin(rotateAngle - 90) * r, yCenter + Math.cos(rotateAngle - 90) * r];
		else if(rotateAngle < 270)
			return [xCenter + Math.cos(rotateAngle - 180) * r, yCenter + Math.sin(rotateAngle - 180) * r];
		else 
			return [xCenter + Math.sin(rotateAngle - 270) * r, yCenter + Math.cos(rotateAngle - 270) * r];
	}
	
	//Клонирование
	this.CloneClass = function(a) {
		var f = function() {};
		f.prototype = a;
		
		var g = new f();
		g.prototype = a;
		
		return g;
	}

	//Клонирование
	this.Clone = function(o) {
		if(!(o.constructor && o.constructor.keys && o.constructor.keys(o)))
			return this.CloneClass(o);

		var newObject = {};
		var keys = o.constructor.keys(o);
		for(var el in keys)
			if(typeof(o[keys[0]]) == 'object')
				newObject[keys[0]] = Clone(o[keys[0]]);
			else
				newObject[keys[0]] = o[keys[0]];

		return newObject;
	}

	//Содержится ли a в b или наоборот
	this.Contains = function(a, b){
		aX = a.X() - a.img.size[0] / 2;
		aY = a.Y() - a.img.size[1] / 2;
		bX = b.X() - b.img.size[0] / 2;
		bY = b.Y() - b.img.size[1] / 2;
		if((aX >= bX && aX <= bX + b.img.size[0] || bX >= aX && bX <= aX + a.img.size[0]) &&
			(aY >= bY && aY <= bY + b.img.size[1] || bY >= aY && bY <= aY + a.img.size[1]))
			return true;
		return false;
	}
	//точка контакта
	this.PointContact = function(a, b){
		
		return [];
	}
	
	//СОдержится ли точка b в a
	this.ContainsPoint = function(a, b){
		bX = (b.X() - b.img.size[0] / 2) || b.x;
		bY = (b.Y() - b.img.size[1] / 2) || b.y;
		if(a.img && a.img.size)
		{
			aX = a.X() - a.img.size[0] / 2;
			aY = a.Y() - a.img.size[1] / 2;
			if(bX >= aX && bX <= aX + a.img.size[0] && bY >= aY && bY <= aY + a.img.size[1])
				return true;
		}
		else if(bX >= a.x && bX <= a.x + a.width && bY >= a.y && bY <= a.y + a.height)
				return true;
		return false;
	}
	
	//Генерация изображения
	this.generateView = function(el){
		if(!el.img.draw) //не прорисовывать
			return ["#", -1, -1];
		
		var res = [el.animate === true ?  "i" : "r", el.X(), el.Y(), el.RotateAngle(), el.CurrentFrame()];
		
		if(el.typeClass == "Animate")
		{
			res.push(el.frimesLife, el.timeLife);
			if(el.residueTimeLife < el.timeLife + 10) //плавное затухание
				res[0] = "t";
		}
		
		if(el.randomStart === true || el.randomStart === 'true') //рандомная первая картинка
		{
			if(el.img.animate === true)
				res[0] = "a";
			else
			{
				res[0] = "r";
				el.CurrentFrame(this.getRandomInt(1, el.img.nframes));
			}
			el.randomStart = false;
		}
		return res;
	}
	
	
	//Рандомное число
	this.getRandomInt = function(min, max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	this.getRandom = function(min, max){
		return Math.random() * (max - min + 1) + min;
	}
	
	
	//Содержится ли пересечение el = [x, y, width, height, id] с одним из объектов массива
	this.ContainsArray = function(el, mas, physics){ //physics - содержание только физических объектов
		for(var i in mas)
			if((physics && mas[i].physics || !physics) && mas[i].id != el[4]) //если это физический и не тот же объект
			{
				var bX = mas[i].X() - mas[i].img.size[0] / 2;
				var bY = mas[i].Y() - mas[i].img.size[1] / 2;
				if((bX >= el[0] && bX <= el[0] + el[2] || el[0] >= bX && el[0] <= bX + mas[i].img.size[0]) &&
					(bY >= el[1] && bY <= el[1] + el[3] || el[1] >= bY && el[1] <= bY + mas[i].img.size[1]))
					return true;
			}
		
		return false;
	}
	
	//Равенство двух массивов
	this.EquallyArrays = function(arr1, arr2){
		if(arr1.length != arr2.length)
			return false;
		
		for(var i in arr1)
			if(arr1[i] != arr2[i])
				return false;
			
		return true;
	}
	
	//Определение в каком квадрате находится объект
	this.LocationQuard = function(el){
		var Location = [];
		Location[0] = this.FormulaLocation(el.X(), el.Y()); //заносим верхний левый угол
		if(el.img)
		{
			var a = this.FormulaLocation(el.X() + el.img.size[0], el.Y()); //верхний правый
			if(Location[0] != a)
				Location.push(a);
			
			a = this.FormulaLocation(el.X(), el.Y() + el.img.size[1]); //нижний левый
			if(Location.indexOf(a) == -1)
				Location.push(a);
			
			a = this.FormulaLocation(el.X() + el.img.size[0], el.Y() + el.img.size[1]); //нижний правый
			if(Location.indexOf(a) == -1)
				Location.push(a);
		}
		
		return Location;
	}
	//Определение в каком квадрате находится объект
	this.LocationQuard2 = function(x, y, width, height){
		var Location = [];
		Location[0] = this.FormulaLocation(x, y); //заносим верхний левый угол
		
		var a = this.FormulaLocation(x + width, y); //верхний правый
		if(Location[0] != a)
			Location.push(a);
		
		a = this.FormulaLocation(x, y + height); //нижний левый
		if(Location.indexOf(a) == -1)
			Location.push(a);
		
		a = this.FormulaLocation(x + width, y + height); //нижний правый
		if(Location.indexOf(a) == -1)
			Location.push(a);
	
		return Location;
	}
	this.FormulaLocation = function(x, y){
		if(x < 0)
			x = 0;
		if(y < 0)
			y = 0;
		
		if(this.SizeMapQuards == 0)
			this.SizeMapQuards = [Math.ceil(this.SizeMap[0] / this.sizeQuard), Math.ceil(this.SizeMap[1] / this.sizeQuard)];
		
		return parseInt(x / this.sizeQuard) + parseInt(y / this.sizeQuard) * this.SizeMapQuards[0];
	}
	
	//Возвращает все объекты находящиеся в выбранном квадрате, рамки которого указаны в arrayQuards
	this.GetGobjectsInQuards = function(arrayQuards, CoordGobjects, view){
		var res = {};
		if(arrayQuards.length == 1) //1 квадрат
		{
			if(CoordGobjects[arrayQuards[0]])
				for(var j in CoordGobjects[arrayQuards[0]])
					if(view)
						res[CoordGobjects[arrayQuards[0]][j].id] = CoordGobjects[arrayQuards[0]][j].view;
					else
						res[CoordGobjects[arrayQuards[0]][j].id] = CoordGobjects[arrayQuards[0]][j];
			return res;	
		}
		
		var last = arrayQuards.pop(); //последний элемент
		for(var i = arrayQuards[0]; i <= last; i += this.SizeMapQuards[0]) //высота
			for(var j = i; j <= i + arrayQuards[1]; j++) //ширина
				if(CoordGobjects[j]) 
					for(var z in CoordGobjects[j])
						if(view)
							res[CoordGobjects[j][z].id] = CoordGobjects[j][z].view;
						else
							res[CoordGobjects[j][z].id] = CoordGobjects[j][z];
		return res;
	}
	
	//Запись в общие координаты местоположение объекта
	this.SetGObjectsInQuars = function(CoordGobjects, el){
		el.coordQuard = this.LocationQuard(el); //граничные квадраты объекта(центра нету, если не вмещается в 1-2 клетки)

		//Изменились ли квадраты местоположения
		var mas = el.coordQuard;
		var masOld = el.lastCoordQuard;
		if(masOld && (mas.length != masOld.length || mas[0] != masOld[0] || mas[mas.length - 1] != masOld[mas.length - 1])) //изменился квадрат
		{
			//console.log(el.id + " | " + el.lastCoordQuard + " | " + el.coordQuard); 
			//удаляем в предыдущих местах объект
			var last = el.lastCoordQuard[el.lastCoordQuard.length - 1]; //последний элемент
			for(var i = el.lastCoordQuard[0]; last > -1 && i <= last; i += this.SizeMapQuards[0]) //высота
			{
				for(var j = i; j <= i + (el.lastCoordQuard[1] ? el.lastCoordQuard[1] % this.SizeMapQuards[0] - i % this.SizeMapQuards[0] : 0); j++) //ширина
				{
					//console.log("delete: " + j);
					delete CoordGobjects[j][el.id];
					if(Object.keys(CoordGobjects[j]).length == 0)
						delete CoordGobjects[j];
				}
			}
		
			if(el.coordQuard.length == 1) //лишь в 1 квадрате
			{
				if(!CoordGobjects[el.coordQuard[0]])
					CoordGobjects[el.coordQuard[0]] = {};
				
				CoordGobjects[el.coordQuard[0]][el.id] = el;
				//console.log("add: " + el.coordQuard[0]);
			}
			else
			{
				var last = el.coordQuard[el.coordQuard.length - 1]; //последний элемент
				
				for(var i = el.coordQuard[0]; i <= last; i += this.SizeMapQuards[0]) //высота
					for(var j = i; j <= i + el.coordQuard[1] % this.SizeMapQuards[0] - i % this.SizeMapQuards[0]; j++) //ширина
					{
						if(!CoordGobjects[j])
							CoordGobjects[j] = {};
						
						//console.log("add: " + j);
						CoordGobjects[j][el.id] = el;
					}
			}
		}
	}
	
	//Запись визуальной части в список изменений
	this.SetGobjectsInChangeQuards = function(ViewChange, el){
		for(var i in el.coordQuard)
		{
			if(!ViewChange[el.coordQuard[i]])
				ViewChange[el.coordQuard[i]] = {};
			ViewChange[el.coordQuard[i]][el.id] = el;
		}
	}
}

module.exports = Helper; //делаем файл модулем